# Animador de Algoritmos

Alunos: Leonardo Kawasaki e Leonardo Lima

## Sobre

Projeto da matéria de Laboratório de Computação II.

Roteiro: https://drive.google.com/open?id=1mxDm5xNC8e_agGXld4bc3yZbQxmUE1Qb

## Tecnologias Utilizadas

Para o desenvolvimento deste projeto foi utilizada a linguagem de programação Java 8 e a IDE 
Intellij IDEA.

## *How To*

Para rodar este projeto em sua máquina é necessário possuir o Java 8 instalado.

Tutorial para o Intellij:

* Clone esse projeto através do comando `git clone`, utilizando a ferramenta Git Bash

* Abra o projeto no Intellij

* Após aberto ele ainda não estará pronto para se executado: 

![tela_1](/uploads/ce42369fd8aa9547a9e30ca5309c6375/tela_1.JPG)

É necessário seguir as próximas imagens:

1 - Vá até "Project Structure"

![tela_2](/uploads/53593e1a6bfa093a81a922e73834c903/tela_2.JPG)

2 - Defina o JDK e o nível de compilação para o Java 8

![tela_3](/uploads/bc961c93faf47b55daa85547f9f8c451/tela_3.JPG)

3 - Indique que a pasta "src" é onde estarão os códigos

![tela_4](/uploads/c455d8a65c97d74e442c48eb1ad0ca0d/tela_4.JPG)

4 - Crie um novo diretório "out" para guardar o código compilado

![tela_5](/uploads/0b798af084eda210b5cc7bb0f082d9ee/tela_5.JPG)

![tela_6](/uploads/d82b6503858f6f129284774f7aea7eba/tela_6.JPG)

5 - Por fim, coloque o caminho deste diretório "out" em "Project Structure" -> "Project" ->
"Project compiler output"

![tela_7](/uploads/e8caa37d649858e8133ea9feab6f0186/tela_7.JPG)

Após isso, o projeto estará pronto para ser executado.
