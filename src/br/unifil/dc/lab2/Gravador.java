package br.unifil.dc.lab2;

import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.awt.Color;

/**
 * Write a description of class Gravador here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Gravador {

    private List<Transparencia> seqGravacoes;

    /**
     * Construtor da classe Gravador
     */
    public Gravador() {
        this.seqGravacoes = new ArrayList<Transparencia>();
    }

    /**
     * Grava a disposição da lista de valores e de cores para desenho.
     * @param lista valores da lista a serem desenhados/pintados
     * @param nome nome da iteração realizada
     */
    public void gravarLista(List<Integer> lista, String nome) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        ListaGravada gravacao = new ListaGravada(copia, cores, nome);
        seqGravacoes.add(gravacao);
    }

    /**
     * Destaca o indice que está sendo trabalhado, pintado-o de outra cor
     * @param lista lista de elementos
     * @param i indice do elemento a ser destacado
     * @param nome nome da iteração feita
     */
    public void gravarIndiceDestacado(List<Integer> lista, int i, String nome) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        cores.set(i, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(copia, cores, nome);
        seqGravacoes.add(gravacao);
    }

    /**
     * Destaca os indices que estão sendo comparados.
     * @param lista lista de elementos a serem modificados
     * @param i primeiro indice a ser destacado
     * @param j segundo indice a ser destacado
     */
    public void gravarComparacaoSimples(List<Integer> lista, int i, int j) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        cores.set(i, Color.GRAY);
        cores.set(j, Color.GRAY);
        ListaGravada gravacao = new ListaGravada(copia, cores, "Comparação");
        seqGravacoes.add(gravacao);
    }

    /**
     * Destaca os indices que estão sendo comparados.
     * @param lista lista de elementos a serem modificados
     * @param i primeiro indice a ser destacado
     * @param j segundo indice a ser destacado
     * @param k terceiro indice a ser destacado
     */
    public void gravarComparacaoComposta(List<Integer> lista, int i, int j, int k) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        cores.set(i, Color.GRAY);
        cores.set(j, Color.GRAY);
        cores.set(k, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(copia, cores, "Comparação");
        seqGravacoes.add(gravacao);
    }

    /**
     * Destaca após a realização de uma troca os indices que foram trocados
     * @param lista lista de elementos a serem modificados
     * @param i primeiro indice a ser destacado
     * @param j segundo indice a ser destacado
     */
    public void gravarPosTrocas(List<Integer> lista, int i, int j) {
        List<Integer> copia = new ArrayList<Integer>(lista);
        List<Color> cores = novaListaColors(lista.size());
        cores.set(i, Color.YELLOW);
        cores.set(j, Color.YELLOW);
        ListaGravada gravacao = new ListaGravada(copia, cores, "Pós-troca");
        seqGravacoes.add(gravacao);
    }

    /**
     * Método para retornar qual filme está sendo mostrado no momento atual.
     * @return o nome do filme atual.
     */
    public ListIterator<Transparencia> getFilme() {
        return seqGravacoes.listIterator();
    }

    /**
     * Cria uma lista de cores que serão trabalhadas para pintar os elementos da lista
     * @param numElems indices em que as cores serão setadas
     * @return a lista de cores, no momento nula, para ser trabalhada nos métodos de pintar.
     */
    private static List<Color> novaListaColors(int numElems) {
        ArrayList<Color> lista = new ArrayList<>(numElems);
        for (; numElems > 0; numElems--) lista.add(null);
        return lista;
    }
}
