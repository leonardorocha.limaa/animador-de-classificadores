package br.unifil.dc.lab2;

import java.util.List;

/**
 * Write a description of class AlgoritmosAnimados here.
 * 
 * @author Leonardo Lima e Leonardo Kawasaki
 * @version 20200425
 */
public class AlgoritmosAnimados {

    /**
     * Desenha a lista de valores estaticamente
     * @param valores valores a serem desenhados
     * @return desenho estático das barras lista, sem alterações.
     */
    public static Gravador listaEstatica(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Valores da lista imutável");
        return anim;
    }

    /**
     * Pesquisa de forma sequencial pela chave dentro da lista de valores.
     * Mostra as iterações de forma ilustrativa, destacando
     * os elementos que estão sendo comparados.
     * @param valores valores a serem verificados
     * @param chave chave que desejamos encontrar na lista
     * @return Objeto Gravador de acordo com a lista e chava passada
     */
    public static Gravador pesquisaSequencial(List<Integer> valores, int chave) {

        // Tive que arrumar o método, ele dava IndexOutOfBoundsException
        // quando a chave não era encontrada.
        // O indice estava sendo incrementado antes do desenho e então
        // caso não existisse a chave, ele tentava escrever no indice valores.size()+1;

        //instancia do gravador no método
        Gravador anim = new Gravador();
        //gravando na lista de valores a disposição inicial da lista
        anim.gravarLista(valores, "Inicio de pesquisa sequencial");

        //contador do while
        int i = 0;
        //destaca o indice inicial da busca
        anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");

        //contador para ir pintando cada indice da busca
        while (i < valores.size() && valores.get(i) != chave) {
            //pinta o indice em que a busca se encontra caso ainda não tenha encontrado a chave
            anim.gravarIndiceDestacado(valores, i, "Pesquisa sequencial");
            i++;
        }

        //verifica se a chave foi encontrada
        if (i < valores.size()) {
            //se while anterior tiver parado antes de acabar signigica que a chave foi encontrada
            //então o indice fica pintado e o programa termina
            anim.gravarIndiceDestacado(valores, i, "Chave encontrada");
        } else {
            //do contrário, a lista volta a ser pintada com a diposição incial
            anim.gravarLista(valores, "Chave não encontrada");
        }
        
        return anim;
    }

    /**
     * Método que executa a pesquisa binária na lista passada como parâmetro.
     * Mostra as iterações de forma ilustrativa, destacando
     * os elementos que estão sendo comparados.
     * @param valores lista que contém os valores que serão iterados.
     * @param chave valor que será pesquisado
     * @return Objeto Gravador de acordo com o valores da lista e a chave.
     */
    public static Gravador pesquisaBinaria(List<Integer> valores, int chave) {
        Gravador anim = new Gravador();
        ordenarElementos(valores);
        anim.gravarLista(valores, "Disposição inicial");

        List<Integer> auxValores = valores;

        int acumulaMeio = 0;
        int destaqueInicial = 0;
        int destaqueFinal = valores.size()-1;
        do {
            int meio = valores.size() / 2;
            anim.gravarComparacaoComposta(auxValores, destaqueInicial, destaqueFinal,
                    (meio + acumulaMeio));
            if (valores.get(meio) == chave) {
                anim.gravarIndiceDestacado(auxValores, (meio+acumulaMeio),
                        "Chave encontrada");
                return anim;
            } else if (chave < valores.get(meio)) {
                valores = valores.subList(0, meio);
                destaqueFinal = meio;
            } else if (chave > valores.get(meio)) {
                acumulaMeio += meio + 1;
                valores = valores.subList(meio + 1, valores.size());
                destaqueInicial = acumulaMeio;
            }
        } while(valores.size() > 0);
        anim.gravarLista(auxValores, "Chave não encontrada");

        return anim;
    }

    /**
     * Método que executa a classificação por bolha na lista passada como parâmetro.
     * Mostra as iterações de forma ilustrativa, destacando
     * os elementos que estão sendo comparados.
     * @param valores Lista a ser classificada, sofre mutação (in-place).
     * @return Objeto Gravador de acordo com a lista passada.
     */
    public static Gravador classificarPorBolha(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        boolean houvePermuta;
        do {
            houvePermuta = false;
            // Sobe a bolha
            for (int i = 1; i < valores.size(); i++) {
                anim.gravarIndiceDestacado(valores, i, "Disposição Atual");
                if (valores.get(i - 1) > valores.get(i)) {
                    anim.gravarComparacaoSimples(valores, i - 1, i);
                    permutar(valores, i - 1, i);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);
        anim.gravarLista(valores, "Disposição final");

        return anim;
    }

    /**
     * Método que executa a classificação por seleção da lista passada como parâmetro.
     * Mostra as iterações de forma ilustrativa, destacando
     * os elementos que estão sendo comparados.
     * @param valores Lista a ser classificada, sofre mutação (in-place).
     * @return Objeto Gravador com a lista passada.
     */
    public static Gravador classificarPorSelecao(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição inicial");

        for (int i = 0; i < valores.size(); i++) {
            anim.gravarIndiceDestacado(valores, i, "Disposição Atual");
            int menorIdx = encontrarIndiceMenorElem(valores, i);
            anim.gravarComparacaoSimples(valores, menorIdx, i);
            permutar(valores, menorIdx, i);
        }
        anim.gravarLista(valores, "Disposição final");

        return anim;
    }

    /**
     * Método que executa a classificação por inserção da lista passada como parâmetro.
     * Mostra as iterações de forma ilustrativa, destacando
     * os elementos que estão sendo comparados.
     * @param valores Lista a ser classificada, sofre mutação (in-place).
     * @return Objeto Gravador de acordo com a lista passada.
     */
    public static Gravador classificarPorInsercao(List<Integer> valores) {
        Gravador anim = new Gravador();
        anim.gravarLista(valores, "Disposição Inicial");
        assert valores != null : "Lista não pode ser nula.";

        for (int i = 1; i < valores.size(); i++) {
            int elemAtual = valores.get(i);
            int posicaoTroca;
            for (posicaoTroca = i; posicaoTroca > 0; posicaoTroca--) {
                anim.gravarComparacaoSimples(valores, posicaoTroca-1, i);
                if (valores.get(posicaoTroca-1) < elemAtual) {
                    break;
                }
                valores.set(posicaoTroca, valores.get(posicaoTroca-1));
                anim.gravarPosTrocas(valores, posicaoTroca, posicaoTroca-1);
            }
            valores.set(posicaoTroca, elemAtual);
            anim.gravarPosTrocas(valores, posicaoTroca, i);
        }
        anim.gravarLista(valores, "Disposição Final");

        return anim;
    }

    /**
     * Ordena os elementos de qualquer lista utilizando o método
     * de ordenação por seleção.
     * @param valores Lista de valores a serem ordenados
     */
    public static void ordenarElementos(List<Integer> valores) {
        for (int i = 0; i < valores.size(); i++) {
            int menorIdx = encontrarIndiceMenorElem(valores, i);
            permutar(valores, menorIdx, i);
        }
    }

    /**
     * Permuta (swap) dois elementos da lista de posição.
     * @param lista Lista cujos elementos serão permutados.
     * @param a Îndice do primeiro elemento a ser permutado.
     * @param b Îndice do outro elemento a ser permutado.
     */
    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a); // permutador = lista[a]
        lista.set(a, lista.get(b)); // lista[a] = lista[b]
        lista.set(b, permutador); // lista[b] = permutador
    }

    /**
     * Encontra o índice do menor elemento da lista.
     * @param lista lista a ser procurada.
     * @return índice do elemento, na escala iniciada em zero.
     */
    private static int encontrarIndiceMenorElem(List<Integer> lista, int idxInicio) {
        int menor = idxInicio;
        for (int i = idxInicio+1; i < lista.size(); i++) {
            if (lista.get(menor) > lista.get(i))
                menor = i;
        }
        return menor;
    }
}
