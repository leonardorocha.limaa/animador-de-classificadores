package br.unifil.dc.lab2;

import java.awt.BasicStroke;
import java.awt.FontMetrics;
import java.util.ArrayList;
import javax.swing.JPanel;
import java.awt.Graphics2D;
import java.awt.Color;
import java.util.List;

/**
 * Write a description of class ListaGravada here.
 * 
 * @author Leonardo Kawasaki e Leonardo Lima
 * @version 20200425
 */
public class ListaGravada implements Transparencia {
    private List<Integer> lista;
    private List<Float> listaProporcional = new ArrayList<>();
    private List<Color> coresIndices;
    private String nome;
    private int maiorElemento;
    private double maiorColuna;
    private double largura;
    private double espacamento;
    private double margem;

    /**
     * Constructor for objects of class ListaGravada
     */
    ListaGravada(List<Integer> lista, List<Color> coresIndices, String nome) {
        this.lista = lista;
        this.nome = nome;
        this.coresIndices = coresIndices;
    }

    public void pintar(Graphics2D graphics2D, JPanel panel) {
        int x = panel.getWidth();
        int y = panel.getHeight();

        acharMaiorElemento(lista);
        compararElementos();
        arrumarProporcoesEspacos(lista, x, y);
        desenhar(graphics2D);
    }

    private void acharMaiorElemento(List<Integer> lista) {
        for (Integer integer : lista) {
            if (integer > maiorElemento) {
                maiorElemento = integer;
            }
        }
    }

    private void compararElementos() {
        for (int i = 0; i < lista.size(); i++) {
            float listTemp = lista.get(i);
            float temp = listTemp / maiorElemento;
            listaProporcional.add(i, temp);
        }
    }

    private void arrumarProporcoesEspacos(List<Integer> lista, int x, int y) {
        double larguraElementosProp = 60.0;
        double distanciaProp = 40.0;
        double margemProp = 15.0;

        larguraElementosProp = larguraElementosProp / lista.size();
        distanciaProp = distanciaProp / lista.size();
        margemProp = margemProp / lista.size();

        largura = ((x / 100.0) * larguraElementosProp);
        espacamento = ((x / 100.0) * distanciaProp);
        margem = ((x / 100.0) * margemProp);

        maiorColuna = y / (y / (y / 2.0));
    }

    private void desenhar(Graphics2D graphics2D) {
        int aux = (int) margem;

        for(int i = 0; i < lista.size(); i++) {
            double alturas = maiorColuna * listaProporcional.get(i);
            double eixoY = 230 + (maiorColuna - alturas);
            nome = lista.get(i).toString();

            graphics2D.setStroke(new BasicStroke(5));
            graphics2D.setColor(Color.BLACK);
            graphics2D.drawRect(aux, (int) eixoY, (int) largura, (int) alturas);
            if(coresIndices.get(i) == null) {
                graphics2D.setColor(Color.BLUE);
            } else {
                graphics2D.setColor(coresIndices.get(i));
            }
            graphics2D.fillRect(aux, (int) eixoY, (int) largura, (int) alturas);

            // Desenha o nome:
            FontMetrics fontMetrics = graphics2D.getFontMetrics();
            double x = aux + (largura / 2) - (fontMetrics.stringWidth(nome) / 2.0);
            double y = (eixoY + alturas + 1) + fontMetrics.getHeight();
            graphics2D.setColor(Color.BLACK);
            graphics2D.drawString(nome, (int) x, (int) y);

            aux += largura + espacamento;
        }
    }
}
